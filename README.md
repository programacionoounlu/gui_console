# GUI Console

GUI Console es un proyecto que permite tener una consola dentro de una interfáz gráfica. El objetivo de este proyecto es poder crear interfaces consola sin el problema de bloqueo existente por la espera de una acción del usuario (ingresar una opción). 

# Modo de uso

El texto se imprime en un `JTextArea` con los diferentes métodos de la clase ConsoleGui: 
    
    public void println(String texto)
    public void insertar(String texto, int fila, int inicio)
    public void reemplazarFila(String texto, int fila)

Los inputs del usuario se toman a través de un `JTextField` y serán informados vía `ObservableManager` a los objetos que estén registrados para escuchar los cambios que se generen cada vez que el usuario ingrese texto a través de este componente. 

La forma de anotarse como observador es implementando la interface `PropertyChangeListener` y suscribirse de la siguiente manera:

    ObservableManager.getInstance().addPropertyChangeListener(IConsoleGui.observableId, IConsoleGui.observableProperty, this);

Donde `this` hace referencia a la clase que implementa `PropertyChangeListener`.

# Ejemplo

El repositorio consta con un ejemplo de uso de la librería `ar.edu.unlu.example`.