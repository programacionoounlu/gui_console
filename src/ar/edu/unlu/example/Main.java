package ar.edu.unlu.example;

import java.awt.EventQueue;

import ar.edu.unlu.consoleView.ConsoleGui;

public class Main {
	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				new SimpleExample(ConsoleGui.getInstance());
			}
		});
		
	}
}
