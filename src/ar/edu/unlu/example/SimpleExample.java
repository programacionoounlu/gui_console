package ar.edu.unlu.example;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.text.BadLocationException;

import ar.edu.unlu.consoleView.IConsoleGui;
import ar.edu.unlu.consoleView.MaxLenghtLineExeeded;
import ar.edu.unlu.observer.ObservableManager;

public class SimpleExample implements PropertyChangeListener {
	
	private IConsoleGui cg;
	
	public SimpleExample(IConsoleGui cg) {
		this.cg = cg;
		ObservableManager.getInstance().addPropertyChangeListener(	IConsoleGui.observableId, 
																	IConsoleGui.observableProperty, 
																	this);
		prepararPantalla();
	}
	
	private void prepararPantalla() {
		try {
			cg.println("                  Estado:          ");
			cg.println("JUEGO:                             ");
			cg.println("-----------------------------------");
			cg.println("Hora:");
		} catch (MaxLenghtLineExeeded e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			cg.insertar("INICIAL", 0, 26);
		} catch (BadLocationException | MaxLenghtLineExeeded e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		
		try {
			cg.insertar("Mayor y menor", 1, 7);
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MaxLenghtLineExeeded e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			cg.reemplazarFila("Hora = 18:04                          9", 3);
		} catch (BadLocationException | MaxLenghtLineExeeded e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String texto = (String) evt.getNewValue();
		try {
			cg.println(texto);
		} catch (MaxLenghtLineExeeded e) {
			e.printStackTrace();
		}
	}
}
