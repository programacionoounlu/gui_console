package ar.edu.unlu.consoleView;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;

import ar.edu.unlu.observer.Notifier;
import ar.edu.unlu.observer.ObservableAlreadyExists;
import ar.edu.unlu.observer.ObservableManager;

public class ConsoleGui extends JFrame implements IConsoleGui{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8125195568154881508L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextArea textArea;
	private JScrollPane scrollPane;
	private Notifier notificador;
	
	private static IConsoleGui instance = new ConsoleGui();

	
	public static IConsoleGui getInstance() {
		return instance;
	}
	
	private ConsoleGui() {
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1010, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		textField = new JTextField();
		contentPane.add(textField, BorderLayout.SOUTH);
		textField.setColumns(10);
		
		
		Font tipoLetra = new Font("Monospaced", Font.PLAIN, 12);
		
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setCaretColor(Color.LIGHT_GRAY);
		textArea.setLineWrap(true);
		textArea.setColumns(13);
		textArea.setFont(tipoLetra);
		textArea.setForeground(Color.WHITE);
		textArea.setBackground(Color.BLACK);
		
		scrollPane = new JScrollPane(textArea);
		contentPane.add(scrollPane, BorderLayout.CENTER);
	
		DefaultCaret caret = (DefaultCaret)textArea.getCaret();
		
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		try {
			notificador = ObservableManager.getInstance().addObservable(IConsoleGui.observableId);
		} catch (ObservableAlreadyExists e) {
			e.printStackTrace();
		}
		
		textField.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ConsoleGui.this.notificador.notifyListeners(IConsoleGui.observableProperty, null, textField.getText());
				textField.setText("");
			}
		});
	}

	@Override
	public void println(String texto) throws MaxLenghtLineExeeded {
		if (texto.length() <= MAX_LENGTH_LINE) {
			textArea.append(texto);
			textArea.append("\n");
		}else
			throw new MaxLenghtLineExeeded("El texto tiene una longitud mas larga que la permitida");
		
	}

	@Override
	public void insertar(String texto, int fila, int inicio) throws BadLocationException, MaxLenghtLineExeeded {
		int offsetFila = textArea.getLineStartOffset(fila);
		int columnaFinal = offsetFila+inicio+texto.length();
		int columnaInicial = offsetFila+inicio;
		if(columnaFinal <= MAX_LENGTH_LINE && textArea.getLineOfOffset(columnaFinal) == fila) {
			textArea.replaceRange(texto,columnaInicial,columnaFinal);
		}else
			throw new MaxLenghtLineExeeded("Se excedió del maximo permitido por fila al insertar");		
		
	}

	@Override
	public void reemplazarFila(String texto, int fila) throws BadLocationException, MaxLenghtLineExeeded {
		int offsetFinal = -1;
		try {
			offsetFinal = textArea.getLineStartOffset(fila+1);
			
		}catch (Exception e) {
			// Pidió reemplazar la ultima linea.
			offsetFinal = textArea.getDocument().getLength();
			
		}
		textArea.replaceRange("\n", textArea.getLineStartOffset(fila), offsetFinal);
		if (texto.length() <= MAX_LENGTH_LINE && offsetFinal != -1) {
			textArea.insert(texto, textArea.getLineStartOffset(fila));
		}else
			throw new MaxLenghtLineExeeded("El texto tiene una longitud mas larga que la permitida");
		
		
	}

}
