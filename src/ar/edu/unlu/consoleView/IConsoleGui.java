package ar.edu.unlu.consoleView;

import java.beans.PropertyChangeListener;

import javax.swing.text.BadLocationException;

import ar.edu.unlu.observer.ObservableManager;

public interface IConsoleGui {
	
	/**
	 * Establece la cantidad máxima de caracteres que se permiten insertar en una linea
	 */
	public final int MAX_LENGTH_LINE = 142;
	
	/**
	 * Clave con la que se debe registrar la clase como observada. Cualquiera que quiera observar
	 * el evento enter del text input debe subscribirse como observador de "IConsoleGui". Para
	 * esto es necesario pedirle a {@link ObservableManager} que agregue un {@link PropertyChangeListener}
	 * para ese observable.
	 * 
	 */
	public static final String observableId = "IConsoleGui";
	
	/**
	 * Las notificaciones de un texto nuevo llegaran con este ID de propiedad
	 */
	public static final String observableProperty = "texto";
	
	/**
	 * Permite insertar una nueva linea, el largo del texto no 
	 * puede superar MAX_LENGTH_LINE. En tal caso lanza la
	 * exception {@link MaxLenghtLineExeeded}.
	 * @param texto
	 */
	public void println(String texto) throws MaxLenghtLineExeeded;
	
	/**
	 * Permite insertar texto a partir de una fila y columna. La fila y columna debe 
	 * existir previamente, es decir, tiene que existir texto en la fila en el rango:
	 * 
	 * 			[columna, columna + texto.size()]
	 * 
	 * Si columna + texto.size() es mayor al largo de la linea o excede MAX_LENGTH_size 
	 * entonces lanza excepcion {@link MaxLenghtLineExeeded}
	 * 
	 * En caso que la fila o posicion final del texto no exista entonces lanza {@link BadLocationException}
	 * 
	 * @param texto
	 * @param fila
	 * @param columna
	 * @throws BadLocationException 
	 * @throws MaxLenghtLineExeeded 
	 */
	public void insertar(String texto, int fila, int columna) throws BadLocationException, MaxLenghtLineExeeded;
	
	/**
	 * Reemplaza el contenido completo de una fila (si esta existe) por el texto pasado como parametro. Si la
	 * fila no existe lanza {@link BadLocationException}. Si texto.length() > MAX_LENGTH_SIZE entonces lanza
	 * {@link MaxLenghtLineExeeded}
	 * @param texto
	 * @param linea
	 */
	public void reemplazarFila(String texto, int fila) throws BadLocationException, MaxLenghtLineExeeded;
	
}
